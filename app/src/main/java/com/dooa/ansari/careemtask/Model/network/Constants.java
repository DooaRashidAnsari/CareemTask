package com.dooa.ansari.careemtask.Model.network;

/**
 * Created by ambre on 4/29/2018.
 */

public class Constants {
    public static final String BASE_URL = "https://api.themoviedb.org/3/movie/";
    public static final String BASE_URL_IMAGE = "https://image.tmdb.org/t/p/w185//";

    // latest was returning only one movie so i changed to upcoming

    public static final String GET_MOVIES_LIST = "upcoming";

    public static final String API_KEY = "a0ae0df44aed19ffcacac3b32976bc83";
    public static final String API_PARAMETER_NAME = "api_key";


    public static final int SERVICE_ID_MOVIES_LIST = 0;
    public static final int SERVICE_ID_MOVIE_DETAILS = 1;


}
