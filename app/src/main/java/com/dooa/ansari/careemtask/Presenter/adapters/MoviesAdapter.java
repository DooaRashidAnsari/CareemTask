package com.dooa.ansari.careemtask.Presenter.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dooa.ansari.careemtask.Model.objectmodels.Movie;
import com.dooa.ansari.careemtask.Presenter.essentails.Helper;
import com.dooa.ansari.careemtask.Presenter.interfaces.IMovieClick;
import com.dooa.ansari.careemtask.R;
import com.dooa.ansari.careemtask.View.MainActivity;

import java.util.ArrayList;

/**
 * Created by ambre on 4/29/2018.
 */

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.CustomViewHolder> implements Filterable {

    private ArrayList<Movie> movies;
    private ArrayList<Movie> filteredList;
    private Context context;
    private IMovieClick movieClick;

    public MoviesAdapter(ArrayList<Movie> movies, Context context, IMovieClick movieClick) {
        this.movies = movies;
        this.filteredList = movies;
        this.context = context;
        this.movieClick = movieClick;
    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CustomViewHolder customViewHolder = null;
        View view = null;
        view = LayoutInflater.from(context).inflate(R.layout.row_movie, parent, false);
        customViewHolder = new CustomViewHolder(view);

        return customViewHolder;
    }

    public ArrayList<Movie> getMovies() {
        return movies;
    }


    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        Movie movie = filteredList.get(position);
        holder.movieName.setText(movie.getMovieName());

        holder.movieDesc.setText(movie.getMovieDetails());
        Helper.loadImage(holder.mainImage, holder.imageProgress, movie.getMoviePoster(), context);

    }


    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    public void setFilteredList(ArrayList<Movie> filteredList) {
        this.filteredList = filteredList;
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView movieName;
        private TextView movieDesc;
        private ImageView mainImage;
        private ProgressBar imageProgress;


        public CustomViewHolder(View itemView) {
            super(itemView);

            movieName = itemView.findViewById(R.id.moviename);
            movieDesc = itemView.findViewById(R.id.desc);
            mainImage = itemView.findViewById(R.id.movieimage);
            imageProgress = itemView.findViewById(R.id.imgprogress);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                Movie movie = filteredList.get(getAdapterPosition());
                movieClick.onMovieClick(movie);
            }
        }
    }
}
