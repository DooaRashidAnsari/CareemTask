package com.dooa.ansari.careemtask.Presenter;

import android.content.Context;

import com.dooa.ansari.careemtask.Model.objectmodels.Movie;
import com.dooa.ansari.careemtask.Presenter.essentails.Helper;
import com.dooa.ansari.careemtask.Presenter.interfaces.IViewBinder;

import java.util.ArrayList;

/**
 * Created by ambre on 4/29/2018.
 */

public class FilterPresenter extends BasePresenter {

    public FilterPresenter(Context context, IViewBinder viewBinder) {
        super(context, viewBinder);
    }

    public ArrayList<Movie> getFilteredList(ArrayList<Movie> list, int day, int month, int year) {
        ArrayList<Movie> filtered = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (Helper.isDateEqual(
                    list.get(i).getReleaseDate()
                    , day, month, year
            )) {
                filtered.add(list.get(i));
            }
        }

        return filtered;
    }
}
