package com.dooa.ansari.careemtask.Presenter.interfaces;

import com.dooa.ansari.careemtask.Model.objectmodels.Movie;

import java.util.ArrayList;

/**
 * Created by ambre on 4/29/2018.
 */

public interface IMoviesListBinder extends IViewBinder {
    public void setMoviesListView(ArrayList<Movie> movies);
}
