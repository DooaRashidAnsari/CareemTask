package com.dooa.ansari.careemtask.Presenter.interfaces;

import com.dooa.ansari.careemtask.Model.objectmodels.Movie;

/**
 * Created by ambre on 4/29/2018.
 */

public interface IMovieDetails extends IViewBinder{
    public void setDataToView(Movie movie);
}
