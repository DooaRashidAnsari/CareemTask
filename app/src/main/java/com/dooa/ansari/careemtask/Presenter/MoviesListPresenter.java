package com.dooa.ansari.careemtask.Presenter;

import android.content.Context;
import android.widget.Toast;

import com.dooa.ansari.careemtask.Model.BaseModel;
import com.dooa.ansari.careemtask.Model.MoviesModel;
import com.dooa.ansari.careemtask.Model.objectmodels.Movie;
import com.dooa.ansari.careemtask.Presenter.interfaces.IMoviesListBinder;
import com.dooa.ansari.careemtask.Presenter.interfaces.IViewBinder;
import com.dooa.ansari.careemtask.R;
import com.dooa.ansari.careemtask.View.MainActivity;

import java.util.ArrayList;

/**
 * Created by ambre on 4/29/2018.
 */

public class MoviesListPresenter extends BasePresenter implements IMoviesListBinder {

    private BaseModel model;
    private IViewBinder binder;
    private ArrayList<Movie> list;
    private FilterPresenter filterPresenter;

    public MoviesListPresenter(Context context, IViewBinder viewBinder) {
        super(context, viewBinder);
        model = new MoviesModel(context, this);
        this.binder = viewBinder;
        this.filterPresenter = new FilterPresenter(context, viewBinder);
    }

    public void getMovies() {
        if (model instanceof MoviesModel) {
            ((MoviesModel) model).fetchLatestMovies();
        }
    }

    @Override
    public void setMoviesListView(ArrayList<Movie> movies) {
        if (movies == null) {
            Toast.makeText(context, context.getString(R.string.datafetchfail), Toast.LENGTH_SHORT)
                    .show();

        } else {
            if (binder instanceof IMoviesListBinder) {
                this.list = movies;
                ((IMoviesListBinder) binder)
                        .setMoviesListView(movies);
            }
        }
    }

    public void filterList(int day, int month, int year) {
        if (list != null) {
            ArrayList<Movie> filteredList = filterPresenter.getFilteredList(list, day, month, year);
            if (binder instanceof IMoviesListBinder) {
                ((IMoviesListBinder) binder)
                        .setMoviesListView(filteredList);
            }
        }
    }
}
