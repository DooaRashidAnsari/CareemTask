package com.dooa.ansari.careemtask.Model;

import android.content.Context;

/**
 * Created by ambre on 4/29/2018.
 */

public class BaseModel {

    Context context;

    public BaseModel(Context context) {
        this.context = context;
    }
}
