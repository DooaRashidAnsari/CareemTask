package com.dooa.ansari.careemtask.Presenter.essentails;

import android.content.Context;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.dooa.ansari.careemtask.Model.network.Constants;
import com.dooa.ansari.careemtask.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ambre on 4/29/2018.
 */

public class Helper {
    public static final String DATE_MOVIE_FORMAT = "yyy-MM-dd";

    public static void loadImage(ImageView imageView, final ProgressBar progressBar, String id, Context context) {
        if (context != null && id != null) {
            String path = new StringBuilder(Constants.BASE_URL_IMAGE)
                    .append(id).toString();
            Picasso.with(context).load(path).into(imageView, new Callback() {
                @Override
                public void onSuccess() {
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    progressBar.setVisibility(View.GONE);
                }
            });
        }

    }

    public static Date getDateObject(String date) {
        SimpleDateFormat format = new SimpleDateFormat(DATE_MOVIE_FORMAT);
        Date parsedDate = null;
        try {
            parsedDate = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return parsedDate;
    }

    public static boolean isDateEqual(String movieDate, int day, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(getDateObject(movieDate));

        return calendar.get(Calendar.MONTH) == month
                && calendar.get(Calendar.YEAR) == year
                && calendar.get(Calendar.DAY_OF_MONTH)
                == day;

    }

}
