package com.dooa.ansari.careemtask.Model;

import android.app.Service;
import android.content.Context;
import android.util.Log;

import com.dooa.ansari.careemtask.Model.network.Constants;
import com.dooa.ansari.careemtask.Model.network.Get;
import com.dooa.ansari.careemtask.Model.network.INetworkRequest;
import com.dooa.ansari.careemtask.Model.network.Pair;
import com.dooa.ansari.careemtask.Model.objectmodels.Movie;
import com.dooa.ansari.careemtask.Presenter.interfaces.IMovieDetails;
import com.dooa.ansari.careemtask.Presenter.interfaces.IMoviesListBinder;
import com.dooa.ansari.careemtask.Presenter.interfaces.IViewBinder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ambre on 4/29/2018.
 */

public class MoviesModel extends BaseModel implements INetworkRequest {

    private IViewBinder binder;

    public MoviesModel(Context context, IViewBinder binder) {
        super(context);
        this.binder = binder;

    }

    public void fetchLatestMovies() {
        ArrayList<Pair> parameters = new ArrayList<>();
        parameters.add(new Pair(Constants.API_PARAMETER_NAME, Constants.API_KEY));
        Get httpMethod = new Get(new StringBuilder(Constants.BASE_URL)
                .append(Constants.GET_MOVIES_LIST).toString(), context, parameters, this, Constants.SERVICE_ID_MOVIES_LIST);
        httpMethod.execute();
    }

    public void fetchMovieDetails(int movieId) {
        ArrayList<Pair> parameters = new ArrayList<>();
        parameters.add(new Pair(Constants.API_PARAMETER_NAME, Constants.API_KEY));
        Get httpMethod = new Get(new StringBuilder(Constants.BASE_URL)
                .append(movieId).toString()
                , context, parameters, this, Constants.SERVICE_ID_MOVIES_LIST);
        httpMethod.execute();

    }


    @Override
    public void onSuccess(String data, int serviceId) {
        if (serviceId == Constants.SERVICE_ID_MOVIES_LIST) {
            if (binder instanceof IMoviesListBinder) {
                ((IMoviesListBinder) binder).setMoviesListView(parseDataForMovies(data));
            }
        } else if (serviceId == Constants.SERVICE_ID_MOVIE_DETAILS) {
            if (binder instanceof IMovieDetails) {
                ((IMovieDetails) binder).setDataToView(parseDataForMovieDetails(data));
            }
        }
    }

    @Override
    public void onFailure(int serviceId) {
        if (serviceId == Constants.SERVICE_ID_MOVIES_LIST) {
            if (binder instanceof IMoviesListBinder) {
                ((IMoviesListBinder) binder).setMoviesListView(parseDataForMovies(null));
            }
        } else if (serviceId == Constants.SERVICE_ID_MOVIE_DETAILS) {
            if (binder instanceof IMovieDetails) {
                ((IMovieDetails) binder).setDataToView(null);
            }
        }
    }

    public ArrayList<Movie> parseDataForMovies(String data) {
        ArrayList<Movie> movies = new ArrayList<>();
        if (data != null) {
            try {
                JSONObject object = new JSONObject(data);
                if (object.has("results")) {
                    JSONArray array = object.getJSONArray("results");
                    for (int i = 0; i < array.length(); i++) {
                        Movie movie = new Movie();
                        JSONObject movieObject = array.getJSONObject(i);
                        movie.setMovieId(movieObject.getInt("id"));

                        if (movieObject.has("title") && !movieObject.isNull("title"))
                            movie.setMovieName(movieObject.getString("title"));

                        if (movieObject.has("overview") && !movieObject.isNull("overview"))
                            movie.setMovieDetails(movieObject.getString("overview"));

                        if (movieObject.has("poster_path") && !movieObject.isNull("poster_path"))
                            movie.setMoviePoster(movieObject.getString("poster_path"));

                        if (movieObject.has("release_date") && !movieObject.isNull("release_date"))
                            movie.setReleaseDate(movieObject.getString("release_date"));


                        movies.add(movie);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return movies;
    }

    public Movie parseDataForMovieDetails(String data) {
        Movie movie = new Movie();
        if (data != null) {
            try {
                Log.v("tab",data);
                JSONObject movieObject = new JSONObject(data);
                movie.setMovieId(movieObject.getInt("id"));

                if (movieObject.has("title") && !movieObject.isNull("title"))
                    movie.setMovieName(movieObject.getString("title"));

                if (movieObject.has("overview") && !movieObject.isNull("overview"))
                    movie.setMovieDetails(movieObject.getString("overview"));

                if (movieObject.has("poster_path") && !movieObject.isNull("poster_path"))
                    movie.setMoviePoster(movieObject.getString("poster_path"));

                if (movieObject.has("release_date") && !movieObject.isNull("release_date"))
                    movie.setReleaseDate(movieObject.getString("release_date"));


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return movie;
    }
}
