package com.dooa.ansari.careemtask.Model.network;

/**
 * Created by ambre on 4/29/2018.
 */

public interface INetworkRequest {
    public void onSuccess(String data,int serviceId);

    public void onFailure(int serviceId);
}
