package com.dooa.ansari.careemtask.Presenter;

import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import com.dooa.ansari.careemtask.Model.MoviesModel;
import com.dooa.ansari.careemtask.Model.objectmodels.Movie;
import com.dooa.ansari.careemtask.Presenter.interfaces.IMovieDetails;
import com.dooa.ansari.careemtask.Presenter.interfaces.IViewBinder;
import com.dooa.ansari.careemtask.R;

/**
 * Created by ambre on 4/29/2018.
 */

public class MovieDetailsPresenter extends BasePresenter implements IMovieDetails {
    public static final String MOVIE_ID = "movie_id";

    private Context context;
    private IViewBinder viewBinder;
    private MoviesModel model;
    private int movieId;

    public MovieDetailsPresenter(Context context, IViewBinder viewBinder) {
        super(context, viewBinder);
        this.context = context;
        this.viewBinder = viewBinder;
        model = new MoviesModel(context, this);
    }

    public void setMovieId(Bundle bundle) {
        if (bundle != null) {
            if (bundle.containsKey(MOVIE_ID)) {
                movieId = bundle.getInt(MOVIE_ID);
            }
        }
    }


    public void getDetails() {
      model.fetchMovieDetails(movieId);
    }


    @Override
    public void setDataToView(Movie movie) {
        if (movie != null) {
            if (viewBinder instanceof IMovieDetails) {
                ((IMovieDetails) viewBinder)
                        .setDataToView(movie);
            }
        } else {
            Toast.makeText(context, context.getString(R.string.datafetchfail), Toast.LENGTH_SHORT)
                    .show();

        }
    }
}
