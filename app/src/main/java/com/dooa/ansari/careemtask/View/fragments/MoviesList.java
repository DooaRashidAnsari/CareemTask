package com.dooa.ansari.careemtask.View.fragments;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.dooa.ansari.careemtask.Model.objectmodels.Movie;
import com.dooa.ansari.careemtask.Presenter.MovieDetailsPresenter;
import com.dooa.ansari.careemtask.Presenter.MoviesListPresenter;
import com.dooa.ansari.careemtask.Presenter.adapters.MoviesAdapter;
import com.dooa.ansari.careemtask.Presenter.interfaces.IMovieClick;
import com.dooa.ansari.careemtask.Presenter.interfaces.IMoviesListBinder;
import com.dooa.ansari.careemtask.R;
import com.dooa.ansari.careemtask.View.BaseActivity;
import com.dooa.ansari.careemtask.View.dialogs.DateDialog;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoviesList extends Fragment implements IMoviesListBinder, IMovieClick, View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private RecyclerView moviesList;
    private MoviesListPresenter presenter;
    private TextView filter;


    public MoviesList() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movies_list, container, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        presenter = new MoviesListPresenter(getActivity(), this);
        initializeViews(view);
        presenter.getMovies();

    }

    private void initializeViews(View view) {
        moviesList = view.findViewById(R.id.movies_list);
        filter = view.findViewById(R.id.filter);
        filter.setOnClickListener(this);
    }

    @Override
    public void setMoviesListView(ArrayList<Movie> movies) {
        MoviesAdapter adapter = new MoviesAdapter(movies, getActivity(), this);
        moviesList.setLayoutManager(new LinearLayoutManager(getActivity()));
        moviesList.setAdapter(adapter);
    }

    @Override
    public void onMovieClick(Movie movie) {
        MovieDetails details = new MovieDetails();
        Bundle bundle = new Bundle();
        bundle.putInt(MovieDetailsPresenter.MOVIE_ID, movie.getMovieId());
        details.setArguments(bundle);
        ((BaseActivity) getActivity()).replaceWithFragment(
                details, "MovieDetails"
        );
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.filter: {
                new DateDialog()
                        .showDialog(getActivity(), this);
            }
            break;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        presenter.filterList(dayOfMonth, month, year);
    }
}
