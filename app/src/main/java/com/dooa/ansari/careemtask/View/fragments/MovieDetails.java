package com.dooa.ansari.careemtask.View.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dooa.ansari.careemtask.Model.objectmodels.Movie;
import com.dooa.ansari.careemtask.Presenter.MovieDetailsPresenter;
import com.dooa.ansari.careemtask.Presenter.interfaces.IMovieDetails;
import com.dooa.ansari.careemtask.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MovieDetails extends Fragment implements IMovieDetails {
    private TextView date, title, description;
    private MovieDetailsPresenter presenter;

    public MovieDetails() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movie_details, container, false);
    }


    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        presenter=new MovieDetailsPresenter(getActivity(),this);
        initializeViews(view);
        presenter.setMovieId(getArguments());
        presenter.getDetails();
    }

    private void initializeViews(View view) {
        date = view.findViewById(R.id.date);
        title = view.findViewById(R.id.name);
        description = view.findViewById(R.id.desc);

    }

    @Override
    public void setDataToView(Movie movie) {

        if (movie.getMovieName() != null)
            title.setText(movie.getMovieName());

        if (movie.getMovieDetails() != null)
            description.setText(movie.getMovieDetails());

        if (movie.getReleaseDate() != null)
            description.setText(movie.getReleaseDate());
    }
}
