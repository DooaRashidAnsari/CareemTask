package com.dooa.ansari.careemtask.Presenter;

import android.content.Context;

import com.dooa.ansari.careemtask.Presenter.interfaces.IViewBinder;

/**
 * Created by ambre on 4/29/2018.
 */

public class BasePresenter {
    protected Context context;
    protected IViewBinder viewBinder;

    public BasePresenter(Context context, IViewBinder viewBinder) {
        this.context = context;
        this.viewBinder = viewBinder;
    }
}
