package com.dooa.ansari.careemtask.Model.network;

import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by ambre on 4/29/2018.
 */

public class Get extends AsyncTask<String, Void, String> {
    private Context context;
    private ArrayList<Pair> parameters;
    private String url;
    private INetworkRequest networkRequest;
    private int serviceId;

    public Get(String baseUrl, Context context, ArrayList<Pair> parameters, INetworkRequest networkRequest, int serviceId) {
        this.context = context;
        this.parameters = parameters;
        this.networkRequest = networkRequest;
        this.serviceId = serviceId;
        url = constructUrl(baseUrl);
    }

    private String constructUrl(String baseUrl) {
        String service = baseUrl + "?";

        for (Pair parameter : parameters) {
            service = service + parameter.getKey() + "=" + parameter.getValue() + "&";
        }

        if (service.endsWith("&")) {
            service = service.substring(0, service.length() - 1);
        }

        return service;
    }


    @Override
    protected String doInBackground(String... params) {
        URL constructedUrl;
        HttpURLConnection connection = null;
        String result;

        try {
            constructedUrl = new URL(url.replaceAll(" ", "%20"));
            connection = (HttpURLConnection) constructedUrl.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            result = convertStreamToString(connection.getInputStream());

            return result;
        } catch (MalformedURLException e) {
            e.printStackTrace();

        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }

        return null;

    }

    private String convertStreamToString(InputStream inputStream) throws IOException {

        String line = null;
        StringBuilder result = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        while ((line = bufferedReader.readLine()) != null) {
            result.append(line);

        }
        return result.toString();

    }

    @Override
    public void onPostExecute(String data) {
        if (data != null) {
            networkRequest.onSuccess(data, serviceId);
        } else {
            networkRequest.onFailure(serviceId);

        }

    }


}
