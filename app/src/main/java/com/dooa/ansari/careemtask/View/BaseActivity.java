package com.dooa.ansari.careemtask.View;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.dooa.ansari.careemtask.R;

/**
 * Created by ambre on 4/29/2018.
 */

public class BaseActivity extends AppCompatActivity {

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        fragmentManager = getSupportFragmentManager();

    }

    public void replaceWithFragment(Fragment fragment, String tag) {
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.commit();
    }


    public void onStop() {
        super.onStop();

    }

    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


}
