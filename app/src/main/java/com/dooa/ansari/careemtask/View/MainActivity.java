package com.dooa.ansari.careemtask.View;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.dooa.ansari.careemtask.R;
import com.dooa.ansari.careemtask.View.fragments.MoviesList;

public class MainActivity extends BaseActivity{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        replaceWithFragment(
                new MoviesList(),"MovieList"
        );
    }
}
